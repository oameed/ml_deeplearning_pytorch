# Deep Learning with [PyTorch](https://pytorch.org/)

This project is PyTorch implementations of a _Generative Adverserial Network (GAN)_.

## References

_Useful Web Tutorials_

[[1].](https://pytorch.org/tutorials/beginner/dcgan_faces_tutorial.html) Inkawhich. _DCGAN Tutorial_  

## PyTorch

### Trained Model Repositories

[\[view\]](https://pytorch.org/hub/) PyTorch Hub  

## Code Statistics
<pre>
github.com/AlDanial/cloc v 1.74  T=0.02 s (461.6 files/s, 29917.7 lines/s)
-------------------------------------------------------------------------------
Language                     files          blank        comment           code
-------------------------------------------------------------------------------
Python                           6             62             71            262
YAML                             2              2              0            224
Markdown                         1             21              0             30
Bourne Shell                     2             10              7             24
-------------------------------------------------------------------------------
SUM:                            11             95             78            540
-------------------------------------------------------------------------------
</pre>

## How to Run
* This project uses [PyTorch](https://pytorch.org/) Version 1.7.0. The `YAML` file for creating the conda environment used to run this project is included in `run/conda` 
* To Run:  
  * `cd` to the project main directory
  *  `./run/sh/train_gan.sh`

## Experiment: v11

Training GAN with MNIST dataset

|     |     |
|:---:|:---:|
|<img src="networks/v11/predictions/predictions.png"  width=929/> | <img src="networks/v11/logs/train/metrics.png" width=640/> |












