#! /bin/bash

source  activate tcpy
cd      run/pytorch

echo ' CREATING PROJECT DIRECTORY '                            'v11'
rm     -rf                                       ../../networks/v11
tar    -xzf  ../../networks/v00.tar.gz -C        ../../networks
mv           ../../networks/v00                  ../../networks/v11
echo ' TRAINING CONVOLUTIONAL GAN ON MNIST '
python train_gan.py   -data MNIST -net v11 -b 128 -epc 25
echo ' GENERATING PREDICTIONS '
python predict_gan.py -data MNIST -net v11


