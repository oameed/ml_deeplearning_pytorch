###################################################
### DEEP LEARNING WITH PYTORCH                  ###
### TRAIN GENERATIVE ADVERSESRIAL NETWORK (GAN) ### 
### by: OAMEED NOAKOASTEEN                      ###
###################################################

import                                                  os
import                                                  sys
sys.path.append(os.path.join(sys.path[0],'..','..','lib'         ))
sys.path.append(os.path.join(sys.path[0],'..','..','lib','models'))
import                                numpy         as  np
import                                torch         as  tc
from   torch.utils.tensorboard import SummaryWriter
from   matplotlib              import pyplot        as  plt
from   paramd                  import                   PATHS, PARAMS
from   inputd                  import                   input_data
from   nndGAN                  import                  (GENERATOR                 ,
                                                        DISCRIMINATOR             ,
                                                        weights_initializer       ,
                                                        z_sample                  ,
                                                        loss_disc                 ,
                                                        loss_genr                 ,
                                                        monitor_train_on_epoch_end,
                                                        checkpt_train_on_epoch_end )

device         =tc.device("cuda:0" if (tc.cuda.is_available() and PARAMS[0][2] > 0) else "cpu")
log_train      =os.path.join (PATHS[2],'train'     )
writer         =SummaryWriter(log_train            )
counter        =0

#############
### MODEL ###
#############
generator       =GENERATOR    (params=PARAMS       ).to(device)
generator.apply               (weights_initializer )

discriminator   =DISCRIMINATOR(params=PARAMS       ).to(device)
discriminator.apply           (weights_initializer )


optim_disc      =tc.optim.Adam(discriminator.parameters(), lr=2e-4, betas=(0.5, 0.999))
optim_genr      =tc.optim.Adam(generator.parameters    (), lr=2e-4, betas=(0.5, 0.999))

#############
### TRAIN ###
#############
Z_sample        =z_sample     ([100,PARAMS[0][6]], device)      # FIXED NOISE FOR MONITORING PREDICTIONS
losses_d        =[]                                             # LISTS       FOR MONITORING LOSSES
losses_g        =[]

print(' LOADING  DATA     ')

dataloader      =input_data   (PARAMS       , 
                               PATHS        , 
                               [(0.5),(0.5)] )

print(' BEGIN    TRAINING ')

for  epoch       in range    (PARAMS[0][4]):
 for batch, data in enumerate(dataloader  ):
  img_real      =data[0].to(device)
  ### TRAIN DISCRIMINATOR
  discriminator.zero_grad()
  Z             =z_sample     ([PARAMS[0][3],PARAMS[0][6]], device)
  img_fake      =generator    (Z                                  )
  logits_real   =discriminator(img_real                           )
  logits_fake   =discriminator(img_fake                           )
  d_loss        =loss_disc    (logits_real  , logits_fake , device)
  d_loss.backward()
  optim_disc.step()
  ### TRAIN GENERATOR
  generator.zero_grad()
  Z             =z_sample     ([PARAMS[0][3],PARAMS[0][6]], device)
  img_fake      =generator    (Z                                  )
  logits_fake   =discriminator(img_fake                           )
  g_loss        =loss_genr    (logits_fake,                 device)
  g_loss.backward()
  optim_genr.step()
  ### COLLECT AND MONITOR BATCH LOSSES
  losses_d.append(d_loss.item())
  losses_g.append(g_loss.item())
  writer.add_scalar('Loss/train/d_loss', d_loss.item(), counter)
  writer.add_scalar('Loss/train/g_loss', g_loss.item(), counter)
  counter+=1
  if epoch==0 and batch==0:
   writer.add_graph(generator    , Z       )
 # writer.add_graph(discriminator, img_real)
 monitor_train_on_epoch_end([epoch,log_train,writer],
                            [img_real[:100]        ],
                            [['d_loss','g_loss'],
                             [losses_d,losses_g]   ],
                            [generator(Z_sample)   ] )          # MONITOR EPOCH LOSSES AND PREDICTIONS
 checkpt_train_on_epoch_end(PATHS                        ,
                            ['generator','discriminator'],
                            [ generator , discriminator ] )     # CHECKPOINT
 print(' FINISHED EPOCH {} '.format(epoch+1))
writer.close()

print(' FINISHED TRAINING ')


