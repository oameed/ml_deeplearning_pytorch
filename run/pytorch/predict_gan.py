####################################################
### DEEP LEARNING WITH PYTORCH                   ###
### GENERATING PREDICTIONS USING A TRAINED MODEL ###
### by: OAMEED NOAKOASTEEN                       ###
####################################################

import                             os
import                             sys
sys.path.append(os.path.join(sys.path[0],'..','..','lib'         ))
sys.path.append(os.path.join(sys.path[0],'..','..','lib','models'))
import                   numpy  as np
import                   torch  as tc
from   matplotlib import pyplot as plt
from   paramd     import           PATHS, PARAMS
from   utilsd     import           plotter

device='cpu'

##################
### LOAD MODEL ###
##################
from nndGAN import (GENERATOR,
                    z_sample  )

ckptPATH   =os.path.join   (PATHS[1],'generator'+'.pt')   
generator  =GENERATOR      (params=PARAMS             )
generator.load_state_dict(tc.load(ckptPATH)         )
generator.eval()

###############
### PREDICT ###
###############
Z          =z_sample ([100,PARAMS[0][6]], device)
predictions=generator(Z                 ).detach()

##############
### EXPORT ###
##############
filename   =os.path.join(PATHS[3],'predictions')
plt.figure()
plt.axis  ('off')
grid       =plotter     ('torchgrid',predictions      )
grid       =np.transpose(grid,(1,2,0)                 )
plt.imshow              (grid                         )
plt.savefig             (filename+'.png', format='png')


