#!/bin/bash

#SBATCH --ntasks=16
#SBATCH --time=48:00:00
#SBATCH --partition=singleGPU
#SBATCH --gres=
#SBATCH --mail-type=BEGIN,FAIL,END
#SBATCH --mail-user=
#SBATCH --job-name=Train_GAN

module load anaconda3
source activate tcgpy

cd $SLURM_SUBMIT_DIR
cd ../pytorch

echo ' CREATING PROJECT DIRECTORY '                            'v11'
rm     -rf                                       ../../networks/v11
tar    -xzf  ../../networks/v00.tar.gz -C        ../../networks
mv           ../../networks/v00                  ../../networks/v11
echo ' TRAINING CONVOLUTIONAL GAN ON MNIST '
python train_gan.py   -data MNIST -net v11 -b 128 -epc 25
echo ' GENERATING PREDICTIONS '
python predict_gan.py -data MNIST -net v11


