####################################
### DEEP LEARNING WITH PYTORCH   ###
### UTILITY FUNCTION DEFINITIONS ###
### by: OAMEED NOAKOASTEEN       ###
####################################

import                                os
import                 numpy       as np
import                 torchvision as tcv
from matplotlib import pyplot      as plt

def plotter(MODE,DATA,CONFIG=[]):
 if MODE =='torchgrid':
  fig=tcv.utils.make_grid(DATA, nrow=10)
 else:
  if MODE=='metrics':
   index =np.array([i/1e3 for i in range(len(DATA[1][0]))])
   fig,ax=plt.subplots()
   for i in range(len(DATA[0])):
    ax.plot(index, get_normalized_array(DATA[1][i]), CONFIG[i], linewidth=2, label=DATA[0][i])
   ax.set_ylim([0,1])
   ax.set_title ('Normalized Training Loss and Metrics ')
   ax.set_xlabel('Iterations (K)'                       )
   ax.legend    ()
   ax.grid      ()
 return fig
 
def get_normalized_array(X):
 x  =np.array(X)
 MAX=np.amax (x)
 return x/MAX


