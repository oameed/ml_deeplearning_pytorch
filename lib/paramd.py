##################################
### DEEP LEARNING WITH PYTORCH ###
### PARAMETER DEFINITIONS      ###
### by: OAMEED NOAKOASTEEN     ###
##################################

import os
import argparse

### DEFINE PARSER
parser=argparse.ArgumentParser() 

### DEFINE PARAMETERS
parser.add_argument('-data', type=str  ,                help='NAME-OF-DATA-TYPE-DIRECTORY',required=True)
parser.add_argument('-net' , type=str  , default='v00', help='NAME-OF-NETWORK-PROJECT'                  )
parser.add_argument('-gpu' , type=int  , default=1    , help='NUMBER OF GPUs'                           )

  # GROUP: HYPER-PARAMETERS FOR TRAINING
parser.add_argument('-b'   , type=int  , default=128  , help='BATCH-SIZE'                               )
parser.add_argument('-epc' , type=int  , default=50   , help='NUMBER-OF-TRAINING-EPOCHS'                )
parser.add_argument('-step', type=int  , default=200  , help='CHECKPOINTING FRQ.'                       )
 # GROUP:  HYPER-PARAMETERS FOR ARCHITECTURES
parser.add_argument('-zd'  , type=int  , default=100  , help='GAN LATENT DIMENSION'                     )

### ENABLE FLAGS
args=parser.parse_args()

### CONSTRUCT PARAMETER STRUCTURES
PATHS  =[os.path.join('..','..','data'                             ),
         os.path.join('..','..','networks',args.net ,'checkpoints' ),
         os.path.join('..','..','networks',args.net ,'logs'        ),
         os.path.join('..','..','networks',args.net ,'predictions' ) ]

PARAMS =[[args.data,
          args.net ,
          args.gpu ,
          args.b   ,
          args.epc ,
          args.step,
          args.zd   ]]

if  args.data  == 'MNIST'      :
 PARAMS.append  ([1,28,28])
 PARAMS.append  (['0','1','2','3','4','5','6','7','8','9'])
else:
 if args.data  ==  'CIFAR10'   :
  PARAMS.append ([3,32,32])
  PARAMS.append (['airplane','automobile','bird','cat','deer','dog','frog','horse','ship','truck'])

# PARAMS        [0][0] : DATASET TYPE
# PARAMS        [0][1] : NETWORK DIRECTORY
# PARAMS        [0][2] : NUMBER OF GPUs
# PARAMS        [0][3] : BATCH SIZE
# PARAMS        [0][4] : TRAINING EPOCHS
# PARAMS        [0][5] : CHECKPOINTING FREQ.
# PARAMS        [0][6] : GAN's LATENT DIMENSION
# PARAMS        [1][*] : DATASET SHAPE
# PARAMS        [3][*] : DATASET CLASSES


