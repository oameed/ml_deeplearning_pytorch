###################################################
### DEEP LEARNING WITH PYTORCH                  ###
### TRAIN GENERATIVE ADVERSESRIAL NETWORK (GAN) ### 
### MODEL & CALLBACK DEFINITIONS                ###
### by: OAMEED NOAKOASTEEN                      ###
###################################################

import                            os
import                 numpy   as np
import                 torch   as tc
from matplotlib import pyplot  as plt
from utilsd     import            plotter

#############
### MODEL ###
#############

class GENERATOR(tc.nn.Module):
        
    def __init__(self, params):
     super().__init__()
     self.params=params
     self.layer_linear_1=tc.nn.Linear         (self.params[0][6], self.features_init_size(self.params[1])                      )
     self.layer_nonlin_2=tc.nn.LeakyReLU      (0.3)
     self.layer_convtr_3=tc.nn.ConvTranspose2d(128, 64               , (3,3), stride=(2,2), padding=(1,1), output_padding=(1,1))
     self.layer_btcnrm_4=tc.nn.BatchNorm2d    (64 )
     self.layer_nonlin_5=tc.nn.LeakyReLU      (0.3                                                                             )
     self.layer_convtr_6=tc.nn.ConvTranspose2d(64 , self.params[1][0], (3,3), stride=(2,2), padding=(1,1), output_padding=(1,1))
     self.layer_btcnrm_7=tc.nn.BatchNorm2d    (1  )
     self.layer_nonlin_8=tc.nn.Tanh           ()
     
    def forward(self, X):
     batch=X.shape[0]       
     x    =self.layer_linear_1(X)
     x    =x.view             (batch,-1,7,7)
     x    =self.layer_nonlin_2(x)
     x    =self.layer_convtr_3(x)
     x    =self.layer_btcnrm_4(x)
     x    =self.layer_nonlin_5(x)
     x    =self.layer_convtr_6(x)
     x    =self.layer_btcnrm_7(x)
     x    =self.layer_nonlin_8(x)
     return x
    
    def features_init_size(self, SHAPE):
     return np.prod([128,int(SHAPE[1]/4),int(SHAPE[1]/4)])

class DISCRIMINATOR(tc.nn.Module):
    
    def __init__(self, params):
     super().__init__()
     self.params=params
     self.layer_conv2d_1=tc.nn.Conv2d     (self.params[1][0], 128, (4,4), stride=(2,2), padding=(1,1))
     self.layer_btcnrm_2=tc.nn.BatchNorm2d(128)
     self.layer_nonlin_3=tc.nn.LeakyReLU  (0.3)
     self.layer_conv2d_4=tc.nn.Conv2d     (128              , 128, (4,4), stride=(2,2), padding=(1,1))
     self.layer_btcnrm_5=tc.nn.BatchNorm2d(128)
     self.layer_nonlin_6=tc.nn.LeakyReLU  (0.3)
     self.layer_linear_7=tc.nn.Linear     (self.features_final_size(self.params[1]), 1               )
     self.layer_nonlin_8=tc.nn.Sigmoid    ()
     
    def forward(self, X):
     batch=X.shape[0]
     x    =self.layer_conv2d_1(X)
     x    =self.layer_btcnrm_2(x)
     x    =self.layer_nonlin_3(x)
     x    =self.layer_conv2d_4(x)
     x    =self.layer_btcnrm_5(x)
     x    =self.layer_nonlin_6(x)
     x    =x.view(batch,-1)
     x    =self.layer_linear_7(x)
     x    =self.layer_nonlin_8(x)
     return x
    
    def features_final_size(self, SHAPE):
     return np.prod([128,int(SHAPE[1]/4),int(SHAPE[1]/4)])

def weights_initializer(X):
 classname = X.__class__.__name__
 if  classname.find('Conv'     ) != -1:
  tc.nn.init.normal_   (X.weight.data, 0.0, 0.02)
 else:
  if classname.find('BatchNorm') != -1:
   tc.nn.init.normal_  (X.weight.data, 1.0, 0.02)
   tc.nn.init.constant_(X.bias.data  , 0        )

def z_sample(SHAPE,DEVICE):
 return tc.randn(SHAPE, device=DEVICE)

def loss_disc(REAL,FAKE, DEVICE):
 crossentropy=tc.nn.BCELoss()
 d_loss_real =crossentropy (REAL, tc.ones_like (REAL, device=DEVICE))
 d_loss_fake =crossentropy (FAKE, tc.zeros_like(FAKE, device=DEVICE))
 d_loss      =d_loss_real+d_loss_fake
 return d_loss

def loss_genr(FAKE, DEVICE):
 crossentropy=tc.nn.BCELoss()
 g_loss      =crossentropy (FAKE, tc.ones_like (FAKE, device=DEVICE))
 return g_loss

##########################
### TRAINING CALLBACKS ###
##########################

def monitor_train_on_epoch_end(CONFIG, INPUTS, LOSSES, PREDICTIONS):
 epoch   =CONFIG[0]
 logdir  =CONFIG[1]
 writer  =CONFIG[2]
 img_real=plotter('torchgrid', INPUTS     [0]           )
 img_fake=plotter('torchgrid', PREDICTIONS[0].detach()  )
 losses  =plotter('metrics'  , LOSSES        ,['b','r'] )
 writer.add_image('Images/train/Real', img_real, epoch+1)
 writer.add_image('Images/train/Fake', img_fake, epoch+1)
 plt.savefig(os.path.join(CONFIG[1],'metrics'+'.png'),format='png')

def checkpt_train_on_epoch_end(PATHS,NAMES,MODELS):
 for i in range(len(NAMES)):
  tc.save(MODELS[i].state_dict(), os.path.join(PATHS[1],NAMES[i]+'.pt'))


