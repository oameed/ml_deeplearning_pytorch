##################################
### DEEP LEARNING WITH PYTORCH ###
### INPUT FUNCTION DEFINITIONS ###
### by: OAMEED NOAKOASTEEN     ###
##################################

import                os
import numpy       as np
import torch       as tc
import torchvision as tcv

def dataset_selector(DATASET,PATH,NORMPRM,TRAIN):
 if DATASET=='MNIST':
  transform=tcv.transforms.Compose([tcv.transforms.ToTensor ()                     ,
                                    tcv.transforms.Normalize(NORMPRM[0],NORMPRM[1]) ])
  dataset  =tcv.datasets.MNIST    ( root     =PATH     ,
                                    download =True     ,
                                    train    =TRAIN    ,
                                    transform=transform )
 else:
  print(' NOT DEVELOPED YET! ')
 return dataset

def input_data(PARAMS, PATHS, NORMPRM, TRAIN=True):
 dataset   =dataset_selector        (PARAMS[0][0], PATHS[0], NORMPRM, TRAIN)
 dataloader=tc.utils.data.DataLoader(dataset                 , 
                                     batch_size =PARAMS[0][3],
                                     drop_last  =True        ,
                                     shuffle    =True        , 
                                     num_workers=0            )
 return dataloader


